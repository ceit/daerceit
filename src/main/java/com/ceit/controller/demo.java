package com.ceit.controller;

import com.ceit.model.Account;
import com.ceit.service.DemoService;
import com.ceit.util.JsonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by 天天 on 2016/10/28.
 */
@Controller
public class demo {
    JsonMapper nonNullBinder = JsonMapper.nonEmptyMapper();
    @Autowired
    private DemoService demoService;

    @RequestMapping(value = "/demo",method = RequestMethod.GET)
    public String demo(){
        return "demo";
    }
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String add(Account account){
        demoService.add(account);
        return "";
    }
}
