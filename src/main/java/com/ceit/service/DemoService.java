package com.ceit.service;

import com.ceit.dao.DemoMapper;
import com.ceit.model.Account;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by 天天 on 2016/11/15.
 */
@Service
public class DemoService {

    @Resource
    private DemoMapper demoMapper;
    public void add(Account account) {
        demoMapper.addAccount(account);
    }
}
